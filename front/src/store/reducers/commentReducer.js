import {FETCH_COMMENT_FAILURE, FETCH_COMMENT_REQUEST, FETCH_COMMENT_SUCCESS} from "../actions/commentActions";

const initialState = {
    comment: [],
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENT_REQUEST:
            return {...state, postsLoading: true};
        case FETCH_COMMENT_SUCCESS:
            return {...state, postsLoading: false, comment: action.comment};
        case FETCH_COMMENT_FAILURE:
            return {...state, postsLoading: false};
        default:
            return state;
    }
};

export default commentsReducer;