import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';
import {historyPush} from "./historyActions";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POST_INFO_SUCCESS = 'FETCH_POST_INFO_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';
export const CREATE_POSTS_REQUEST = "CREATE_POSTS_REQUEST";
export const CREATE_POSTS_SUCCESS = "CREATE_POSTS_SUCCESS";
export const CREATE_POSTS_FAILURE = "CREATE_POSTS_FAILURE";

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchPostInfoSuccess = post => ({type: FETCH_POST_INFO_SUCCESS, post});
export const fetchPostsFailure = () => ({type: FETCH_POSTS_FAILURE});

export const createPostsSuccess = () => ({type: CREATE_POSTS_SUCCESS});
export const createPostsRequest = (posts) => ({type: CREATE_POSTS_REQUEST, posts});
export const createPostsFailure = (error) => ({type: CREATE_POSTS_FAILURE, error});

export const fetchPosts = () => {
    return async dispatch => {
        try {
            dispatch(fetchPostsRequest());
            const response = await axiosApi.get('/posts');
            dispatch(fetchPostsSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsFailure());
            NotificationManager.error('Could not fetch products');
        }
    }
};

export const fetchPostInfo = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchPostsRequest());
            const response = await axiosApi.get('/posts/' + id);
            dispatch(fetchPostInfoSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsFailure());
            NotificationManager.error('Could not fetch products');
        }
    }
};

export const createPost = (postData) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            dispatch(historyPush("/login"));
        }
        try {
            dispatch(createPostsRequest());
            await axiosApi.post("/posts", postData, { headers: { Authorization: token } });
            dispatch(createPostsSuccess());
            dispatch(historyPush("/"));
        } catch (e) {
            dispatch(createPostsFailure(e));
        }
    };
};

