import React from 'react';
import {Container, CssBaseline} from "@material-ui/core";
import {Switch, Route} from 'react-router-dom';
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import PostForm from "./components/PostForm/PostForm";
import NewPost from "./containers/NewPost/NewPost";
import Posts from "./containers/Posts/Posts";
import PostInfo from "./containers/PostInfo/PostInfo";


const App = () => (
    <>
      <CssBaseline/>
      <header>
          <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Posts} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
              <Route path="/new" component={NewPost} />
              <Route path="/posts/:id" component={PostInfo} />
          </Switch>
        </Container>
      </main>
    </>
);

export default App;
