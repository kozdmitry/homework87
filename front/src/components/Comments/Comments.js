import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {createComment, fetchComment} from "../../store/actions/commentActions";
import {useDispatch} from "react-redux";

const Comments = ({id}) => {
    const dispatch = useDispatch();
    const [state, setState] = useState({
        text: ''
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    useEffect((id) => {
        dispatch(fetchComment(id));
    },[dispatch]);

    const onCommentFormSubmit = (e) => {
        e.preventDefault();
        dispatch(createComment(state));
    };
    return (
        <>
            <form onSubmit={onCommentFormSubmit}>
                <Grid container direction="column" spacing={3}>
                    <Grid item xs>
                        <h2>Comment</h2>
                    </Grid>
                    <Grid item xs>
                        <TextField
                            fullWidth
                            multiline
                            rows={3}
                            variant="outlined"
                            id="comment"
                            label="Text"
                            name="text"
                            value={state.comment}
                            onChange={inputChangeHandler}
                            required
                        />
                    </Grid>
                    <Grid item xs>
                        <Button type="submit" color="primary" variant="contained">
                            Add
                        </Button>
                    </Grid>
                </Grid>
            </form>
            
        </>
    );
};

export default Comments;