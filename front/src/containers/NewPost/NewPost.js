import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";
import {createPost} from "../../store/actions/postsActions";
import FormElement from "../../components/Form/FormElement";
import FileInput from "../../components/Form/FileInput";
import {Button} from "@material-ui/core";

const NewPost = () => {
    const dispatch = useDispatch();
    const [post, setPost] = useState({
        title: "",
        description: "",
        image: "",
    });

    const inputChangeHandler = (e) => {
        const { name, value } = e.target;

        setPost((prev) => ({ ...prev, [name]: value }));
    };

    const submitFormHandler = (e) => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(post).forEach((key) => {
            formData.append(key, post[key]);
        });

        dispatch(createPost(formData));
    };

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];

        setPost((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    return (
        <>
            <Grid container spacing={1} direction="column" component="form" onSubmit={submitFormHandler}>
                <Grid item xs>
                    <FormElement
                        label="Title"
                        type="text"
                        onChange={inputChangeHandler}
                        name="title"
                        value={post.title}
                    />
                </Grid>
                <Grid item xs>
                    <FormElement
                        label="Description"
                        type="text"
                        onChange={inputChangeHandler}
                        name="description"
                        value={post.description}
                    />
                </Grid>
                <Grid item>
                    <FileInput name="image" label="Image" onChange={fileChangeHandler} />
                </Grid>
                <Grid item xs>
                    <Button variant="contained" type="submit" color="primary">
                        Create Post
                    </Button>
                </Grid>

            </Grid>
        </>
    );
};

export default NewPost;