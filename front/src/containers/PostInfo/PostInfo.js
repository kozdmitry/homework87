import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {CardMedia, Paper} from "@material-ui/core";
import {fetchPostInfo} from "../../store/actions/postsActions";
import Comments from "../../components/Comments/Comments";

const useStyles = makeStyles((theme) => ({
    itemMessage: {
        border: "2px solid grey",
        margin: "10px",
        padding: "5px",
    },
    cover: {
        width: '250px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
}));

const PostInfo = (props) => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.post);
    const user = useSelector((state) => state.users.user)
    const classes = useStyles();
    console.log(post);
    console.log(user);


    useEffect(() => {
        dispatch(fetchPostInfo(props.match.params.id))
    }, [dispatch, props.match.params.id]);


    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Paper className={classes.itemMessage}>
                    <h4>{post.title}</h4>
                    <b>Author: {post.author}</b>
                    <p>{post.description}</p>
                    <p>Date: {post.datetime}</p>
                    <CardMedia
                        className={classes.cover}
                        image={'http://localhost:8000/' + post.image}
                    />
                </Paper>
                {user ? <Comments id={post.id}/> : null}
            </Grid>
        </>
    );
};

export default PostInfo;