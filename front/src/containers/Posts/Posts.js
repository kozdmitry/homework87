import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {fetchPosts} from "../../store/actions/postsActions";
import PostItem from "./PostItem";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Posts = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);
    console.log(posts);
    // const loading = useSelector(state => state.posts.posts);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);


    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justify="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4"></Typography>
                </Grid>
            </Grid>
            <Grid item container spacing={1}>
                {posts.map((post) => (
                    <PostItem
                        key={post._id}
                        id={post._id}
                        title={post.title}
                        description={post.description}
                        image={post.image}
                        date={post.date}
                        author={post.author}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Posts;