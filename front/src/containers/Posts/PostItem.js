import React, {useEffect} from 'react';
import {CardMedia, IconButton, Paper} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchPostInfo} from "../../store/actions/postsActions";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    itemMessage: {
        border: "2px solid grey",
        margin: "10px",
        padding: "5px",
    },
    cover: {
        width: '250px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
}));


const PostItem = (props) => {
    const dispatch = useDispatch();

    const classes = useStyles();
    return (

        <>
            <Grid container direction="column" spacing={2}>
                <Grid item xs component={Link} to={'/posts/' + props.id}>
                    <Paper className={classes.itemMessage}>
                        <h4>{props.title}<p>      {props.date}</p></h4>
                        <p>Info: {props.description}</p>
                        <b>Author: {props.author}</b>

                        <CardMedia
                            className={classes.cover}
                            image={'http://localhost:8000/' + props.image}
                        />
                    </Paper>
                </Grid>
            </Grid>
        </>
    );
};

export default PostItem;