const express = require("express");
const { nanoid } = require("nanoid");
const Posts = require("../models/Post");
const auth = require("../middleware/auth");
const multer = require("multer");
const Post = require("../models/Post");
const config = require("../config");
const path = require("path");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({ storage });

router.get("/", async (req, res) => {
    try {
        const post = await Post.find().sort({ datetime: -1 });
        res.send(post);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/:id", async (req, res) => {
    try {
        const post = await Post.findOne({ _id: req.params.id });
        res.send(post);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
    try {
        const postData = { ...req.body, author: req.user._id, datetime: new Date().toISOString() };

        if (req.file) {
            postData.image = "uploads/" + req.file.filename;
        }

        const post = new Post(postData);
        await post.save();
        res.send(post);
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;

