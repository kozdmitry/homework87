const express = require("express");
const { nanoid } = require("nanoid");
const Comment = require("../models/Comment");

const router = express.Router();

router.get("/:id", async (req, res) => {
    try {
        const comment = await Comment.find({ post: req.params.id }).populate("author");
        res.send(comment);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", async (req, res) => {
    const commentData = req.body;
    try {
        const comment = new Comment(commentData);
        comment.datetime = new Date().toISOString();
        await comment.save();
        res.send(comment);
    } catch (e) {
        res.sendStatus(500);
    }
});
module.exports = router;
