const mongoose = require('mongoose');
const config = require("./config");
const Post = require("./models/Post");
const User = require("./models/User");
const {nanoid} = require("nanoid");


const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    };

    await Post.create({
        title: 'New BMW X5M',
        author: '6081b75b69c80f03964cbd29',
        description: 'It’s the M car the world wants right now, not the one it needs.',
        image: "/fixtures/X5M.jpg"
    }, {
        author: "6081b75b69c80f03964cbd29",
        title: 'New G63AMG',
        description: 'It looks like the new 2021 S-Class will get the second generation of MBUX with a Tesla-like iPad plopped on the dash.',
        image: "/fixtures/g63.jpg"
    });

    // await Product.create({
    //     title: 'Intel Core i7',
    //     price: 300,
    //     category: cpuCategory,
    //     image: 'fixtures/cpu.jpg'
    // }, {
    //     title: 'Seagate Barracuda 3TB',
    //     price: 100,
    //     category: hddCategory,
    //     image: 'fixtures/hdd.jpg'
    // });

    await User.create({
        username: 'user',
        password: '1qaz@WSX29',
        token: nanoid()
    }, {
        username: 'admin',
        password: '1qaz@WSX29',
        token: nanoid()
    });

    await mongoose.connection.close();
};

run().catch(console.error);