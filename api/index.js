const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const exitHook = require('async-exit-hook');
const users = require('./app/users');
const posts = require('./app/posts');
const comments = require('./app/comments');
const config = require ("./config");



const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/users', users);
app.use('/posts', posts);
app.use('/comments', comments);





const port = 8000;


const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async (callback) => {
        await mongoose.disconnect();
        console.log('disconnect');
        callback();
    });
};

run().catch(console.error)



